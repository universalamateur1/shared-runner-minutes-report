import requests
import gitlab
import argparse
import csv
import json

# group cursor is used to iterate over months. The report will pull the last 12 months
# project cursor is used to iterate over project nodes in a given month
def request_group_minutes(gitlab, token, group_id, project_page, has_project_object, group_cursor = "", project_cursor = ""):
    print("[Info] Querying project minutes page %s" % project_page)
    projects = []
    api_url = gitlab + "/api/graphql"
    headers = {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    if has_project_object:
        query = """
            query {
              ciMinutesUsage(namespaceId: "gid://gitlab/Group/%s", first: 1 , after:"%s") {
                nodes {
                  minutes,
                  monthIso8601,
                  projects(after:"%s") {
                    nodes {
                      minutes
                      project {
                        fullPath
                      }
                    }
                    pageInfo {
                      endCursor
                      hasNextPage
                    }
                  }
                }
                pageInfo {
                  endCursor
                  hasNextPage
                }
              }
            }""" % (group_id, group_cursor, project_cursor)
    else:
        query = """
            query {
              ciMinutesUsage(namespaceId: "gid://gitlab/Group/%s", first: 1 , after:"%s") {
                nodes {
                  minutes,
                  monthIso8601,
                  projects(after:"%s") {
                    nodes {
                      minutes
                      name
                    }
                    pageInfo {
                      endCursor
                      hasNextPage
                    }
                  }
                }
                pageInfo {
                  endCursor
                  hasNextPage
                }
              }
            }""" % (group_id, group_cursor, project_cursor)
    try:
        r = requests.post(api_url, json={'query': query}, headers = headers)
        r.raise_for_status()
        if "data" in r.json():
            return r.json()
        else:
            print("[ERROR] There was a problem with the query")
            print(r.json())
            return {}
    except Exception as e:
        print("[ERROR] Could not retrieve query!")
        print(e)
        return {}

# usage data format is
# {
#   "$group": {
#     "$month_date": {
#       "minutes": minutes,
#       "projects": [{ "minutes", "full_path"}]
#       "group_path":group.full_path
#     }
#   }
# }
# returns ["group_id","group_path","month_iso","group_minutes","project","project_minutes"]
def flatten(usage_data, has_project_object):
    rows = []
    for group in usage_data:
        for month in usage_data[group]:
            row = [group]
            row.append(usage_data[group][month]["group_path"])
            row.append(month)
            row.append(usage_data[group][month]["minutes"])
            if usage_data[group][month]["projects"]:
                for project in usage_data[group][month]["projects"]:
                    project_row = row.copy()
                    if has_project_object:
                        project_row.append(project["project"]["fullPath"])
                    else:
                        project_row.append(project["name"])
                    project_row.append(project["minutes"])
                    rows.append(project_row)
            else:
                row.append("")
                row.append("0")
                rows.append(row)
    return rows

parser = argparse.ArgumentParser(description='Get runner minutes report for the instance')
parser.add_argument('token', help='GraphQL API token')
parser.add_argument('--gitlab', help='GitLab instance url', default="https://gitlab.com")
parser.add_argument('--group', help='Only fetch minutes in a particular group. Needs group ID.')
args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token, retry_transient_errors=True)

gitlab_url = args.gitlab.strip("/")

groups = []
group_data = {}
usage_data = {}
has_project_object = False

version = gl.version()[0]
version = version.split(".")

# project object introduced in 15.6: https://docs.gitlab.com/ee/api/graphql/reference/#ciminutesprojectmonthlyusage
if int(version[0]) >= 15 and int(version[1]) > 5:
    has_project_object = True

print("[Info] Getting group metadata")
if not args.group:
    try:
        groups = gl.groups.list(iterator=True, top_level_only=True)
    except Exception as e:
        print("[Error] Unable to list groups")
        print(e)
else:
    try:
        group = gl.groups.get(args.group)
        groups = [group]
    except Exception as e:
        print("[Error] Unable to get group %s" % args.group)
        print(e)

for group in groups:
    print("[Info] Getting shared runner usage for %s" % group.full_path)
    usage_data[str(group.id)] = {}
    # we are querying a nested object of group CI minutes usage -> projects CI minutes usage
    # lower complexity by limiting group usage to 1 result == 1 month, then iterate over project pages
    group_cursor = ""
    for month in range(1,12):
        count = 0
        has_next_page = True
        project_cursor = ""
        print("[Info] Querying month %s for group %s" % (month, str(group.id)))
        # this iterates project nodes
        while has_next_page:
            count += 1
            query_results = request_group_minutes(gitlab_url, args.token, str(group.id), count, has_project_object, group_cursor, project_cursor)
            if query_results["data"]["ciMinutesUsage"]["nodes"]:
                group_cursor = query_results["data"]["ciMinutesUsage"]["pageInfo"]["endCursor"]
                # due to the way we are querying, there will only be one node here
                month_date = query_results["data"]["ciMinutesUsage"]["nodes"][0]["monthIso8601"]
                minutes = query_results["data"]["ciMinutesUsage"]["nodes"][0]["minutes"]
                # looks like projects can be null. Huh.
                if query_results["data"]["ciMinutesUsage"]["nodes"][0]["projects"]:
                    project_cursor = query_results["data"]["ciMinutesUsage"]["nodes"][0]["projects"]["pageInfo"]["endCursor"]
                    has_next_page = query_results["data"]["ciMinutesUsage"]["nodes"][0]["projects"]["pageInfo"]["hasNextPage"]
                    projects = query_results["data"]["ciMinutesUsage"]["nodes"][0]["projects"]["nodes"]
                else:
                    has_next_page = False
                    projects = []
                if month_date not in usage_data[str(group.id)]:
                    usage_data[str(group.id)][month_date] = {"minutes": minutes, "projects" : projects, "group_path":group.full_path}
                else:
                    usage_data[str(group.id)][month_date]["projects"].extend(projects)
            else:
                has_next_page = False
#print(json.dumps(usage_data, indent=2))

print("[Info] Printing report")
csv_data = flatten(usage_data, has_project_object)

with open("shared_runner_usage_report.csv","w") as outfile:
    writer = csv.writer(outfile, delimiter="\t")
    header = ["group_id","group_path", "month_iso","group_minutes","project","project_minutes"]
    writer.writerow(header)
    for row in csv_data:
        writer.writerow(row)
