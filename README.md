# Shared runner minutes report

Create a `CSV` report for [shared runner minutes usage](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#view-cicd-minutes-used-by-a-group). Reports for a single group as well as all groups on a self-managed instance are supported.

Queries the last 12 months of usage.

## Usage

`python3 shared_runner_minutes_report.py $GIT_TOKEN --group sciops`

**Parameters:**

* `$GIT_TOKEN`: Personal Access Token with `api` permissions, able to read all groups you want to report on. For GitLab.com, a top group owner token should be used. For self-managed instances, an instance admin token should be used.
* `--gitlab`: Optional GitLab instance url, defaults to `https://gitlab.com`
* `--group`: Optional GitLab group path / ID to limit the report on a single group. Not using this parameter will query all groups visibile to the `$GIT_TOKEN` on the instance.

## Report format

The script creates a CSV file `shared_runner_minutes_report.py` with the following format:

|   group_id  |   group_path  |   month_iso   |   group_minutes  |   project                         |   project_minutes  |
|-------------|---------------|---------------|------------------|-----------------------------------|--------------------|
|   7621122   |   topgroup    |   2022-12-01  |   1              | topgroup/path/to/project1         |   1                |
|   7621122   |   topgroup    |   2022-11-01  |   46             | topgroup/path/to/project1         |   34               |
|   7621122   |   topgroup    |   2022-11-01  |   46             | topgroup/path/to/another/project2 |   11               |

As you can see, it is not normalized and can contain the same (group, month) multiple times to account for the project details. In this example, row 2 and 3 have the same group and date information (first 4 columns), but the 46 minutes where used by 2 separate projects.

## How it works

The script is using both the REST API (for basic group listing and group metadata, like ID and path), as well as the [GraphQL API `Query.ciMinutesUsage`](https://docs.gitlab.com/ee/api/graphql/reference/#queryciminutesusage). Because of the nested nature of the query, which returns iterable `MonthlyUsage` nodes as well as iterable `ProjectUsage` nodes, the script iterates with a separate API call for each month and group.

Thus, the number of calls the script executes (in instance mode) is:

```
( number of groups in the instance / 100 )   #get group metadata
+ number of groups in the instance * 12 * max((number of projects consuming minutes / 100), 1)
```

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems.